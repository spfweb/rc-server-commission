#!/bin/sh

cd

test -f ./saved-config.sh && source ./saved-config.sh

if test -z "${REGISTRATION_TOKEN}"
then

  read -p "Enter CI registration token: " REGISTRATION_TOKEN
  read -p "Enter CI tag(s) (comma separated): " RUNNER_TAG_LIST

  DEFAULT_RUNNER_NAME="${RUNNER_TAG_LIST}"
  read -p "Enter CI description (without date suffx) [${DEFAULT_RUNNER_NAME}]: " RUNNER_NAME
  RUNNER_NAME=${RUNNER_NAME:-${DEFAULT_RUNNER_NAME}}

  read -p "Enter application name: " APP_NAME

  DEFAULT_PERSISTENT_PATH="/opt/${APP_NAME}"
  read -p "Enter path for persistent files [${DEFAULT_PERSISTENT_PATH}]: " PERSISTENT_PATH
  PERSISTENT_PATH="${PERSISTENT_PATH:-${DEFAULT_PERSISTENT_PATH}}"

  read -p "Enter database scheme (e.g. mysql2 or postgres): " DATABASE_SCHEME
  read -p "Enter database host: " DATABASE_HOST

  DEFAULT_DATABASE_USER="${APP_NAME}"
  read -p "Enter database user [${DEFAULT_DATABASE_USER}]: " DATABASE_USER
  DATABASE_USER="${DATABASE_USER:-${DEFAULT_DATABASE_USER}}"

  DEFAULT_DATABASE_NAME="${APP_NAME}_production"
  read -p "Enter database name [${DEFAULT_DATABASE_NAME}]: " DATABASE_NAME
  DATABASE_NAME=${DATABASE_NAME:-${DEFAULT_DATABASE_NAME}}

  # Remember settings so you can run this again to update pakcages

  cat <<EOF > ./saved-config.sh
export REGISTRATION_TOKEN="${REGISTRATION_TOKEN}"
export RUNNER_TAG_LIST="${RUNNER_TAG_LIST}"
export RUNNER_NAME="${RUNNER_NAME}"
export PERSISTENT_PATH="${PERSISTENT_PATH}"
export DATABASE_SCHEME="${DATABASE_SCHEME}"
export DATABASE_HOST="${DATABASE_HOST}"
export DATABASE_USER="${DATABASE_USER}"
export DATABASE_NAME="${DATABASE_NAME}"
EOF

fi

export RUNNER_NAME=${RUNNER_NAME}_`date +"%Y%m%d"`

DISTRO="`awk -F= '/^ID=/{print $2}' /etc/os-release`"

sudo_c=sudo

if test "${DISTRO}" = "ubuntu"
then

    cat <<EOF > ./swupd
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo apt-get -y autoremove
EOF

    chmod +x ./swupd

    # Allow Docker to use aufs storage drivers

    $sudo_c apt-get -y update

    $sudo_c apt-get -y install \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common

    # Docker GPG key

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    # Install Docker

    the_ppa=https://download.docker.com/linux/ubuntu

    if ! grep -q "^deb .*$the_ppa" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    else
        echo $the_ppa already added as a repository
    fi

    $sudo_c apt-get -y update
    $sudo_c apt-get -y install docker-ce

    # Allow this user to admininster Docker
    $sudo_c usermod -aG docker ${SUDO_USER:-${USER}}

elif test "${DISTRO}" = "alpine"
then

    cat <<EOF > ./swupd
sudo apk update
sudo apk upgrade
EOF

    chmod +x ./swupd

    $sudo_c sh -c 'echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories'
    $sudo_c apk update
    $sudo_c apk add nano docker
    $sudo_c rc-update add docker boot
    $sudo_c service docker start

    # Allow this user to admininster Docker
    $sudo_c addgroup ${SUDO_USER:-${USER}} docker

else

    echo "Linux distro unknown: '${DISTRO}'"
    exit 1

fi

# It may take a while before docker is ready
while ! test -e /var/run/docker.sock
do
  echo '...waiting for docker to be ready'
  sleep 1
done

# Run gitlab-runner in Docker container

$sudo_c docker stop gitlab-runner || true
$sudo_c docker rm gitlab-runner || true

# Pull updated version if there is one
$sudo_c docker pull gitlab/gitlab-runner:alpine

$sudo_c docker run -d --name gitlab-runner \
    --restart unless-stopped \
    -v /opt/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:alpine

# ... and register it ...

$sudo_c docker exec -i gitlab-runner gitlab-runner register -n \
    --url https://gitlab.com/ \
    --registration-token ${REGISTRATION_TOKEN} \
    --tag-list ${RUNNER_TAG_LIST} \
    --name ${RUNNER_NAME} \
    --executor docker \
    --docker-image "docker:latest" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock

# remove any old runners that no longer exist

$sudo_c docker exec -it gitlab-runner gitlab-runner verify --delete

# Create the secrets file

if test -f "${PERSISTENT_PATH}/secrets/secrets.env"
then
  echo "Secrets already exist, not overwriting"
else
  echo "Creating new secrets"
  $sudo_c mkdir -p ${PERSISTENT_PATH}/secrets || true
  $sudo_c touch ${PERSISTENT_PATH}/secrets/secrets.env
  $sudo_c sh -c "echo \"export SECRET_KEY_BASE=`openssl rand -hex 64`\" > ${PERSISTENT_PATH}/secrets/secrets.env"
  $sudo_c sh -c "echo \"export DATABASE_SCHEME=${DATABASE_SCHEME}\" >> ${PERSISTENT_PATH}/secrets/secrets.env"
  $sudo_c sh -c "echo \"export DATABASE_HOST=${DATABASE_HOST}\" >> ${PERSISTENT_PATH}/secrets/secrets.env"
  $sudo_c sh -c "echo \"export DATABASE_NAME=${DATABASE_NAME}\" >> ${PERSISTENT_PATH}/secrets/secrets.env"
  $sudo_c sh -c "echo \"export DATABASE_USER=${DATABASE_USER}\" >> ${PERSISTENT_PATH}/secrets/secrets.env"
  $sudo_c sh -c "echo \"export DATABASE_PASSWORD=`openssl rand -hex 24`\" >> ${PERSISTENT_PATH}/secrets/secrets.env"
  $sudo_c sh -c "echo \"export DATABASE_URL=\\\${DATABASE_SCHEME}://\\\${DATABASE_USER}:\\\${DATABASE_PASSWORD}@\\\${DATABASE_HOST}/\\\${DATABASE_NAME}\" >> ${PERSISTENT_PATH}/secrets/secrets.env"
fi

# Don't forget the secrets!

echo ""
echo "**************************************************"
echo "If you are running this a second time, don't"
echo "forget to remove the old runner on gitlab.com"
echo "**************************************************"

echo ""
echo "**************************************************"
echo "Before you trigger the first deployment"
echo "confirm your secrets are good and you can login to"
echo "the database."
echo "**************************************************"
